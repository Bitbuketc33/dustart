using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class patrulla : MonoBehaviour
{

    public NavMeshAgent NavMeshAgent;
    public Vector3 destinationpoint;
    public Transform destinationn;

    private int currentDestinationIndex;

    public List<Transform> destinationogbje;
    // Start is called before the first frame update
    void Start()
    {
        NavMeshAgent.SetDestination(destinationogbje[0].transform.position);
        currentDestinationIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
        //if (transform.position == destinationogbje[0].position)
        if (NavMeshAgent.remainingDistance <= 0.1f) 
        {
            currentDestinationIndex += 1;
            if (currentDestinationIndex >= destinationogbje.Count)
            {
                currentDestinationIndex = 0;
            }
            NavMeshAgent.SetDestination(destinationogbje[currentDestinationIndex].position);
            
        }
    }
}
